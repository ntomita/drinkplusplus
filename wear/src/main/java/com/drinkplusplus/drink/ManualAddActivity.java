package com.drinkplusplus.drink;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.drinkplusplus.drink.db.DrinkEntry;
import com.drinkplusplus.drink.db.HistoryDbHelper;

import java.util.Calendar;

public class ManualAddActivity extends Activity {
    final String TAG = this.getClass().getSimpleName();

    private TextView mTextView;
    private ImageView mWaterView;
    private ImageView mGlassView;
    //private GestureDetector mGestureDetector;

    public static int val = 1;
    static float startY = 0;
    static float endY = 0;

    static float initialY;
    static float lowestY;

    private Context mContext = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_add);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);
                mTextView.setX(160f);
                mTextView.setY(25f);
                mWaterView = (ImageView) stub.findViewById(R.id.water_image);
                initialY = 240f; //mWaterView.getY()+100;
                lowestY = 90f; //mWaterView.getY();
                mWaterView.setX(0f);
                mWaterView.setY(240f);

                mGlassView = (ImageView) findViewById(R.id.glass_image);
                mGlassView.setX(0f);
                mGlassView.setY(40f);

                FloatingActionButton addButton = (FloatingActionButton) stub.findViewById(R.id.manual_add_icon);
                addButton.setX(260f);
                addButton.setY(140f);


            }
        });

        //GestureListener mGestureListener = new GestureListener();
        //mGestureDetector = new GestureDetector(this, mGestureListener);

        //LinearLayout mLinearLayout = (LinearLayout) findViewById(R.id.manual_add_view);
        stub.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:{
                        Log.d(TAG, String.valueOf(event.getY()));
                        startY = event.getY();
                        break;
                    }
                    case MotionEvent.ACTION_MOVE:{
                        Log.d(TAG, String.valueOf(event.getY()));
                        endY = event.getY();
                        float deltaY = startY - endY;
                        val += (int)deltaY;
                        if (val <= 0){
                            val = 1;
                        }


                        /*
                        float nextY = mWaterView.getY() - deltaY;
                        if (nextY > lowestY && nextY < initialY){
                            mWaterView.setY(nextY);
                        }
                        Log.d("initialY:", String.valueOf(initialY));
                        Log.d("lowestY:", String.valueOf(lowestY));
                        Log.d("NextY:", String.valueOf(nextY));
                        */
                        if (val > 300){
                            mWaterView.setY(240f - (300f+0.001f)/2f);
                        }else{
                            mWaterView.setY(240f - (val+0.001f)/2f);
                        }

                        //mWaterView.setLayoutParams(new LinearLayout.LayoutParams(mWaterView.getWidth(),(int) (250f - (val+0.001f)/3f)));
                        //LinearLayout ll = (LinearLayout) findViewById(R.id.manual_add_view);
                        //ll.removeView(mWaterView);
                        //ll.addView(mWaterView);


                        startY = endY;
                        mTextView.setText(String.valueOf(val) + " ml");
                        break;
                    }
                }
                return true;
            }
        });




    }

    public void onAddClicked(View view) {
        HistoryDbHelper dbHelper = new HistoryDbHelper(mContext);
        Calendar now = Calendar.getInstance();
        DrinkEntry entry = new DrinkEntry();
        entry.setDate(now.getTimeInMillis());
        entry.setAmount(val);
        dbHelper.addEntry(entry);
        dbHelper.close();

        Toast.makeText(mContext, "New Record Added!", Toast.LENGTH_SHORT).show();
        finish();
    }



    /*
    *   TOUCH EVENT
    * */

    /*
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mGestureDetector.onTouchEvent(event);

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:{
                Log.d(TAG, String.valueOf(event.getY()));
                break;
            }
            case MotionEvent.ACTION_MOVE:{
                Log.d(TAG, String.valueOf(event.getY()));
                break;
            }
        }
        return super.onTouchEvent(event);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 120;


        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD
                            && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {

                        } else {
                            mWaterView.
                        }
                    }
                    result = true;
                }
                result = true;
            } catch (Exception exception) {
                exception.printStackTrace();
            }

            return result;
        }

    }
    */

}
