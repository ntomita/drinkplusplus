package com.drinkplusplus.drink;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.ArrayMap;
import android.support.wearable.view.WatchViewStub;
import android.view.View;
import android.widget.TextView;

import com.drinkplusplus.drink.db.DrinkEntry;
import com.drinkplusplus.drink.db.HistoryDbHelper;

import java.util.ArrayList;

public class MainActivity extends Activity {

    public static TextView mTextView;
    private Context mContext;
    private FloatingActionButton mAddButton;

    int total=0;

    public static WatchViewStub stub;

    @Override
    protected void onResume() {
        super.onResume();

        if (stub != null && mTextView != null){
            HistoryDbHelper dbHelper = new HistoryDbHelper(mContext);
            ArrayList<DrinkEntry> entries = dbHelper.queryToday();
            dbHelper.close();
            total = 0;
            for (DrinkEntry entry: entries){
                total += entry.getAmount();
            }
            mTextView.setText("Today: " + String.valueOf(total) + " ml");

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);
                mTextView.setText("Today: " + String.valueOf(total) + " ml");

            }
        });

    }

    public void onAddClicked(View view) {
        Intent i = new Intent(mContext, ManualAddActivity.class);
        startActivity(i);


    }



}
