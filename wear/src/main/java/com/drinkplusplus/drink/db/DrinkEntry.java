package com.drinkplusplus.drink.db;

/**
 * Created by Naofumi on 2/27/16.
 */
import java.util.Date;

public class DrinkEntry {
    private int mAmount;
    private long mDate;

    public int getAmount() {
        return mAmount;
    }
    public void setAmount(int amount) {
        mAmount = amount;
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long date) {
        mDate = date;
    }
}
