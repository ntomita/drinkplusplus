package com.drinkplusplus.drink.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Naofumi on 2/27/16.
 */
public class HistoryDbHelper extends SQLiteOpenHelper {
    Context mContext;
    private static String DATABASE_NAME = "HistoryDB";
    private static int DATABASE_VERSION = 1;

    private static String TABLE_NAME = "Records";
    private static String COLUMN_AMOUNT = "amount";
    private static String COLUMN_DATE = "date";

    public HistoryDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
        "(" + COLUMN_DATE + " INTEGER, " + COLUMN_AMOUNT + " INTEGER" + " )");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public ArrayList<DrinkEntry> queryAll(){
        SQLiteDatabase db = this.getReadableDatabase();
        db.beginTransaction();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        ArrayList<DrinkEntry> entries = new ArrayList<>();
        if (cursor.getCount() > 0){
            try{
                //cursor.moveToFirst();
                while(cursor.moveToNext()){
                    DrinkEntry entry = new DrinkEntry();
                    entry.setDate(cursor.getLong(cursor.getColumnIndex(COLUMN_DATE)));
                    entry.setAmount(cursor.getInt(cursor.getColumnIndex(COLUMN_AMOUNT)));
                    entries.add(entry);
                }
                db.setTransactionSuccessful();
            }catch (SQLiteException e){
                e.printStackTrace();
            }finally {
                db.endTransaction();
                cursor.close();
                db.close();
            }
        }

        return entries;
    }

    public ArrayList<DrinkEntry> queryToday(){
        // GET CURRENT TIME
        // AND SET IT AT THE START OF THE DAY
        Calendar today = Calendar.getInstance();
        today.set(Calendar.YEAR, today.get(Calendar.YEAR));
        today.set(Calendar.MONTH, today.get(Calendar.MONTH));
        today.set(Calendar.DAY_OF_MONTH, today.get(Calendar.DAY_OF_MONTH));
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);



        SQLiteDatabase db = getReadableDatabase();
        db.beginTransaction();
        Cursor cursor = db.query(TABLE_NAME, null, COLUMN_DATE + " > " + today.getTimeInMillis(), null, null, null, null);
        ArrayList<DrinkEntry> entries = new ArrayList<DrinkEntry>();
        if (cursor.getCount() > 0){
            try{
                //cursor.moveToFirst();
                while(cursor.moveToNext()){
                    DrinkEntry entry = new DrinkEntry();
                    entry.setDate(cursor.getLong(cursor.getColumnIndex(COLUMN_DATE)));
                    entry.setAmount(cursor.getInt(cursor.getColumnIndex(COLUMN_AMOUNT)));
                    entries.add(entry);
                }
                db.setTransactionSuccessful();
            }catch (SQLiteException e){
                e.printStackTrace();
            }finally {
                db.endTransaction();
                cursor.close();
                db.close();
            }
        }

        return entries;
    }

    public ArrayList<DrinkEntry> queryLastWeek(){
        return null;
    }

    public long addEntry(DrinkEntry entry){
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        ContentValues values = new ContentValues();
        values.put(COLUMN_DATE, entry.getDate());
        values.put(COLUMN_AMOUNT, entry.getAmount());
        long i = db.insert(TABLE_NAME, null, values);
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        return i;
    }

}
